package main

import (
	"fmt"
	"gitee.com/mryy1996/parse-apk/src"
	"io/ioutil"
)

func main() {

	//a := src.NewApk("file/0a66ccd80463ba275fa935089fa182b8.apk")
	//a := src.NewApk("file/ac884cd414f66944fc582424377b1228.apk")
	a := src.NewApk("file/com.happyelements.AndroidAnimal.qq_128.apk")

	info, ee := a.Parse()

	if ee != nil {

		fmt.Println(ee)

		return
	}

	//app名称
	fmt.Println(info.Label)

	//目标dsk
	fmt.Println(info.TargetSdk, info.TargetSdkName)

	//最新运行sdk
	fmt.Println(info.MinSdk, info.MinSdkName)

	//权限列表
	fmt.Println(info.PermissionList)

	//版本号
	fmt.Println(info.VersionCode, info.VersionName)

	//文件大小、文件md5
	fmt.Println(info.Size, info.Md5)

	//保存icon
	ioutil.WriteFile("33.png", info.Icon, 0644)

}
